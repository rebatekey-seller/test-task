import { createStore } from 'vuex';

const store = new createStore({
    state: {
        title: "Our travels",
        menuItems: ["All countries", "Italy", "Germany", "Greece", "Turkey", "Spain", "Portugal"]
    },
    getters: {},
    mutations: {},
    actions: {},
});

export default store;